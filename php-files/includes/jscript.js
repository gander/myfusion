/* 
------------------------------------------
	Flipbox written by CrappoMan
	simonpatterson@dsl.pipex.com
------------------------------------------
*/
function flipBox(who) {
	var tmp; 
	if (document.images['b_' + who].src.indexOf('_on') == -1) { 
		tmp = document.images['b_' + who].src.replace('_off', '_on');
		document.getElementById('box_' + who).style.display = 'none';
		document.images['b_' + who].src = tmp;
	} else { 
		tmp = document.images['b_' + who].src.replace('_on', '_off');
		document.getElementById('box_' + who).style.display = 'block';
		document.images['b_' + who].src = tmp;
	} 
}

function addText(elname, wrap1, wrap2, formname) {
	if (formname=='undefined' || !formname) formname = 'inputform';
	if (document.selection) { // for IE 
		var str = document.selection.createRange().text;
		document.forms[formname].elements[elname].focus();
		var sel = document.selection.createRange();
		sel.text = wrap1 + str + wrap2;
		return;
	} else if ((typeof document.forms[formname].elements[elname].selectionStart) != 'undefined') { // for Mozilla
		var txtarea = document.forms[formname].elements[elname];
		var selLength = txtarea.textLength;
		var selStart = txtarea.selectionStart;
		var selEnd = txtarea.selectionEnd;
		var oldScrollTop = txtarea.scrollTop;
		//if (selEnd == 1 || selEnd == 2)
		//selEnd = selLength;
		var s1 = (txtarea.value).substring(0,selStart);
		var s2 = (txtarea.value).substring(selStart, selEnd)
		var s3 = (txtarea.value).substring(selEnd, selLength);
		txtarea.value = s1 + wrap1 + s2 + wrap2 + s3;
		txtarea.selectionStart = s1.length;
		txtarea.selectionEnd = s1.length + s2.length + wrap1.length + wrap2.length;
		txtarea.scrollTop = oldScrollTop;
		txtarea.focus();
		return;
	} else {
		insertText(elname, wrap1 + wrap2);
	}
}

function insertText(elname, what, formname) {
	if (formname=='undefined' || !formname) formname = 'inputform';
	if (document.forms[formname].elements[elname].createTextRange) {
		document.forms[formname].elements[elname].focus();
		document.selection.createRange().duplicate().text = what;
	} else if ((typeof document.forms[formname].elements[elname].selectionStart) != 'undefined') { // for Mozilla
		var tarea = document.forms[formname].elements[elname];
		var selEnd = tarea.selectionEnd;
		var txtLen = tarea.value.length;
		var txtbefore = tarea.value.substring(0,selEnd);
		var txtafter =  tarea.value.substring(selEnd, txtLen);
		var oldScrollTop = tarea.scrollTop;
		tarea.value = txtbefore + what + txtafter;
		tarea.selectionStart = txtbefore.length + what.length;
		tarea.selectionEnd = txtbefore.length + what.length;
		tarea.scrollTop = oldScrollTop;
		tarea.focus();
	} else {
		document.forms[formname].elements[elname].value += what;
		document.forms[formname].elements[elname].focus();
	}
}

function show_hide(msg_id) {
	msg_id.style.display = msg_id.style.display == 'none' ? 'block' : 'none';
}

function check_input_text_length(inputID, maxSize, infoID){
	if (typeof(infoID)=='undefined' || !infoID) infoID = false;
	else if (inputID == infoID) infoID = infoID + '_length_info';
	try {
		var text = document.getElementById(inputID);
		if (text.value.length>maxSize) text.value = text.value.substr(0,maxSize);
	} catch (e) {return;}
	if (infoID){
		try {
			document.getElementById(infoID).innerHTML = (maxSize-text.value.length);
		} catch (e) {}
	}
}