<?php
/*---------------------------------------------------+
| PHP-Fusion 6 Content Management System
+----------------------------------------------------+
| Copyright � 2002 - 2006 Nick Jones
| http://www.php-fusion.co.uk/
+----------------------------------------------------+
| Released under the terms & conditions of v2 of the
| GNU General Public License. For details refer to
| the included gpl.txt file or visit http://gnu.org
+----------------------------------------------------*/
require_once "maincore.php";
require_once "subheader.php";
require_once "side_left.php";
include LOCALE.LOCALESET."members-profile.php";
include LOCALE.LOCALESET."user_fields.php";

if (isset($_POST['update_profile'])) require_once INCLUDES."update_profile_include.php";

opentable($locale['440']);
if (iMEMBER) {
	if ($userdata['user_birthdate']!="0000-00-00") {
		$user_birthdate = explode("-", $userdata['user_birthdate']);
		$user_month = number_format($user_birthdate['1']);
		$user_day = number_format($user_birthdate['2']);
		$user_year = $user_birthdate['0'];
	} else {
		$user_month = 0; $user_day = 0; $user_year = 0;
	}
	$offset_list = "";
	$user_im = im_user($userdata,true);
	for ($i=-13;$i<17;$i++) {$offset_list .= "<option value='".$i."'".($i == $userdata['user_offset'] ? " selected" : "").">".showdate("shortdate", time()+(($i-$userdata['user_offset'])*3600))." (".($i>0?"+":"").$i.")</option>\n";}
	echo "<form name='inputform' method='post' action='".FUSION_SELF."' enctype='multipart/form-data'>\n";
	echo "<table align='center' cellpadding='0' cellspacing='0'>\n";
	if (isset($_GET['update_profile'])) {
		echo "<tr>\n<td colspan='2' class='tbl'>".$locale['441']."<br><br>\n</td>\n</tr>\n";
	}
	echo "<tr>
<td class='tbl'>".$locale['u001']."<span style='color:#ff0000'>*</span></td>
<td class='tbl'>".($settings['allow_username_change']?"<input type='text' name='user_name' value='".$userdata['user_name']."' maxlength='30' class='textbox' style='width:210px;'>":"<b>".$userdata['user_name']."</b>")."</td>
</tr>
<tr>
<td class='tbl'>".$locale['u003']."</td>
<td class='tbl'><input type='password' name='user_newpassword' maxlength='20' class='textbox' style='width:210px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['u004']."</td>
<td class='tbl'><input type='password' name='user_newpassword2' maxlength='20' class='textbox' style='width:210px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['u005']."<span style='color:#ff0000'>*</span></td>
<td class='tbl'><input type='text' name='user_email' value='".$userdata['user_email']."' maxlength='100' class='textbox' style='width:210px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['u006']."</td>
<td class='tbl'><input type='radio' name='user_hide_email' value='1'".($userdata['user_hide_email'] == "1" ? " checked='checked'" : "").">".$locale['u007']."
<input type='radio' name='user_hide_email' value='0'".($userdata['user_hide_email'] == "0" ? " checked='checked'" : "").">".$locale['u008']."</td>
</tr>
<tr>
<td class='tbl'>".$locale['u009']."</td>
<td class='tbl'><input type='text' name='user_location' value='".$userdata['user_location']."' maxlength='50' class='textbox' style='width:200px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['u010']." <span class='small2'>(mm/dd/yyyy)</span></td>
<td class='tbl'><select name='user_day' class='textbox'>\n<option value='0'>--</option>\n";
	for ($i=1;$i<=31;$i++) echo "<option".($user_day == $i ? " selected='selected'" : "").">$i</option>\n";
	echo "</select>\n<select name='user_month' class='textbox'>\n<option value='0'>--</option>\n";
	$months = explode("|", $locale['months']);
	for ($i=1;$i<=12;$i++) echo "<option value='$i'".($user_month == $i ? " selected='selected'" : "").">".$months[$i]."</option>\n";
	echo "</select>\n<select name='user_year' class='textbox'>\n<option value='0'>----</option>\n";
	for ($i=1900;$i<=date("Y");$i++) echo "<option".($user_year == $i ? " selected='selected'" : "").">$i</option>\n";
	echo "</select>
</td>
</tr>
<tr>
<td class='tbl'>".$locale['myf_0002']." 1:</td>
<td class='tbl'>".im_select('user_aim_select', $user_im[0]['name']).": <input type='text' name='user_aim' value='".$user_im[0]['value']."' maxlength='16' class='textbox' style='width:100px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['myf_0002']." 2:</td>
<td class='tbl'>".im_select('user_icq_select', $user_im[1]['name']).": <input type='text' name='user_icq' value='".$user_im[1]['value']."' maxlength='15' class='textbox' style='width:100px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['myf_0002']." 3:</td>
<td class='tbl'>".im_select('user_msn_select', $user_im[2]['name']).": <input type='text' name='user_msn' value='".$user_im[2]['value']."' maxlength='100' class='textbox' style='width:100px;'></td>
</tr>
<tr>
<td class='tbl'>".$locale['myf_0002']." 4:</td>
<td class='tbl'>".im_select('user_yahoo_select', $user_im[3]['name']).": <input type='text' name='user_yahoo' value='".$user_im[3]['value']."' maxlength='100' class='textbox' style='width:100px;'></td>
</tr>\n";
	if ($settings['allow_theme_change']){
		$theme_files = makefilelist(THEMES, ".|..", true, "folders");
		array_unshift($theme_files, "Default");
		echo "<tr>
<td class='tbl'>".$locale['u015']."</td>
<td class='tbl'><select name='user_theme' class='textbox' style='width:210px;'>
".makefileopts($theme_files, $userdata['user_theme'])."
</select></td>
</tr>\n";
	}
	echo "<tr>
<td class='tbl'>".$locale['u016']."</td>
<td class='tbl'><select name='user_offset' class='textbox' style='width:175px;'>
$offset_list</select></td>
</tr>\n";
	if ($userdata['user_avatar']=="") echo "<tr>
<td class='tbl'>".$locale['u017']."</td>
<td class='tbl'>
<input type='file' name='user_avatar' class='textbox' style='width:210px;'><br>
<span class='small2'>".$locale['u018']."</span><br>
<span class='small2'>".sprintf($locale['u022'], parsebytesize($settings['avatar_max_kbytes']*1024), $settings['avatar_max_width'], $settings['avatar_max_height'])."</span>
</td>
</tr>\n";
	echo "<tr>
<td valign='top' class='tbl'>".$locale['u020']."</td>
<td class='tbl'>
<textarea name='user_sig' id='user_sig' rows='5' cols='60' class='textbox' onKeyDown='check_input_text_length(this.id, ".$settings['sig_maxsize'].", this.id)' onKeyUp='check_input_text_length(this.id, ".$settings['sig_maxsize'].", this.id)'>".$userdata['user_sig']."</textarea><br>
<input type='button' value='b' class='button' style='font-weight:bold;width:25px;' onClick=\"addText('user_sig', '[b]', '[/b]'); check_length();\">
<input type='button' value='i' class='button' style='font-style:italic;width:25px;' onClick=\"addText('user_sig', '[i]', '[/i]'); check_length();\">
<input type='button' value='u' class='button' style='text-decoration:underline;width:25px;' onClick=\"addText('user_sig', '[u]', '[/u]'); check_length();\">
<input type='button' value='url' class='button' style='width:30px;' onClick=\"addText('user_sig', '[url]', '[/url]'); check_length();\">
<input type='button' value='mail' class='button' style='width:35px;' onClick=\"addText('user_sig', '[mail]', '[/mail]'); check_length();\">
<input type='button' value='img' class='button' style='width:30px;' onClick=\"addText('user_sig', '[img]', '[/img]'); check_length();\">
<input type='button' value='center' class='button' style='width:45px;' onClick=\"addText('user_sig', '[center]', '[/center]'); check_length();\">
<input type='button' value='small' class='button' style='width:40px;' onClick=\"addText('user_sig', '[small]', '[/small]'); check_length();\">
#<span id='user_sig_length_info'>0</span>
</td>
</tr>
<tr>
<td align='center' colspan='2' class='tbl'><br>\n";
	if ($userdata['user_avatar']) {
		echo $locale['u017']."<br>\n<img src='".IMAGES."avatars/".$userdata['user_avatar']."' alt='".$locale['u017']."'><br>
<input type='checkbox' name='del_avatar' value='y'> ".$locale['u019']."
<input type='hidden' name='user_avatar' value='".$userdata['user_avatar']."'><br><br>\n";
	}
	echo "<input type='hidden' name='user_hash' value='".$userdata['user_password']."'>
<input type='submit' name='update_profile' value='".$locale['460']."' class='button'></td>
</tr>
<tr>
<td align='left' colspan='2' class='tbl'><br>
<fieldset><legend>".$locale['u020']."</legend>".($userdata['user_sig'] ? nl2br(parseubb(parsesmileys($userdata['user_sig']))) : "<center><i><small>".$locale['myf_0003']."</small></i></center>")."</fieldset>
</td>
</tr>
</table>
</form>
<script language='JavaScript' type='text/javascript'>
check_input_text_length('user_sig', ".$settings['sig_maxsize'].", 'user_sig_length_info');
</script>\n";
	closetable();
} else {
	echo "<center><br>\n".$locale['003']."<br>\n<br></center>\n";
	closetable();
}

require_once "side_right.php";
require_once "footer.php";
?>