<?php
/*---------------------------------------------------+
| Admin Panel
+----------------------------------------------------+
| Copyright (c) 2006 Gander
| http://www.gander.4cms.pl/
+----------------------------------------------------+
| Released under the terms & conditions of v2 of the
| GNU General Public License. For details refer to
| the included gpl.txt file or visit http://gnu.org
+----------------------------------------------------*/
if (iADMIN) {
	
	@include_once LOCALE.LOCALESET."admin/main.php";
	
	openside($locale['083']);
	echo "<center>\n";
	
	for ($i=1; $i<5; $i++) {
		$options = "";
		$result = dbquery("SELECT * FROM ".$db_prefix."admin WHERE admin_page = '".$i."' ORDER BY admin_title ASC");
		$rows = dbrows($result);
		if ($rows != 0) {
			while ($data = dbarray($result)) {
				if (checkrights($data['admin_rights']) && $data['admin_link'] != "reserved") {
					$options .= "<option value='".ADMIN.$data['admin_link'].$aidlink."'>".$data['admin_title']."</option>\n";
				}
			}
		}
		if ($options != ""){
			echo "<form action='".ADMIN."index.php".$aidlink."&amp;pagenum=".$i."'>\n"
			."<select onchange='window.location.href=this.value' style='width:100%;' class='textbox'>\n"
			."<option value='".ADMIN."index.php".$aidlink."&amp;pagenum=".$i."' style='font-style:italic;' selected>".$locale['ac0'.$i]."</option>\n"
			.$options
			."</select>\n"
			."</form>\n";
		}
	}
	
	echo "</center>\n";
	closeside();
}
?>