<?php
/*---------------------------------------------------+
| PHP-Fusion 6 Content Management System
+----------------------------------------------------+
| Copyright � 2002 - 2006 Nick Jones
| http://www.php-fusion.co.uk/
+----------------------------------------------------+
| Released under the terms & conditions of v2 of the
| GNU General Public License. For details refer to
| the included gpl.txt file or visit http://gnu.org
+----------------------------------------------------*/
if (!defined("IN_FUSION")) { header("Location: ../../index.php"); exit; }
if ($settings['allow_theme_change']){
	$fusion_themes = makefilelist(THEMES, ".|..", true, "folders");
	array_unshift($fusion_themes, "Default");
	if (iMEMBER){
		if (isset($_POST['fusion_theme_switch'])){
			$fusion_theme_switch = trim($_POST['fusion_theme_switch']);
			if (!in_array($fusion_theme_switch,$fusion_themes)) $fusion_theme_switch = "Default";
			dbquery("UPDATE ".DB_PREFIX."users SET user_theme='".$fusion_theme_switch."' WHERE user_id='".$userdata['user_id']."'");
			redirect(FUSION_SELF.(FUSION_QUERY?"?".str_replace("&amp;", "&", FUSION_QUERY):""),"script");
			exit();
		} else {
			openside("User Theme Switcher");
			echo "<center><form action='".FUSION_SELF.(FUSION_QUERY?"?".FUSION_QUERY:"")."' method='post'><select name='fusion_theme_switch' class='textbox' onChange='this.form.submit()'>\n";
			foreach ($fusion_themes as $fusion_theme) echo "<option".($userdata['user_theme']==$fusion_theme?" selected='selected' style='font-weight:bold'":"").">".$fusion_theme."</option>\n";
			echo "</select></form></center>\n";
			closeside();
		}
	} else {
		if (isset($_POST['fusion_theme_switch'])){
			$fusion_theme_switch = trim($_POST['fusion_theme_switch']);
			if (!in_array($fusion_theme_switch,$fusion_themes)) $fusion_theme_switch = $settings['theme'];
			header("P3P: CP='NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM'");
			setcookie("fusion_guest_theme", $fusion_theme_switch, time() + 3600*24*30, "/", "", "0");
			redirect(FUSION_SELF.(FUSION_QUERY?"?".str_replace("&amp;", "&", FUSION_QUERY):""),"script");
			exit();
		} else {
			openside("Guest Theme Switcher");
			echo "<center><form action='".FUSION_SELF."' method='post'><select name='fusion_theme_switch' class='textbox' onChange='this.form.submit()'>\n";
			foreach ($fusion_themes as $fusion_theme) echo "<option".(GUEST_THEME==$fusion_theme?" selected='selected' style='font-weight:bold'":"").">".$fusion_theme."</option>\n";
			echo "</select></form></center>\n";
			closeside();
		}
	}
}
?>