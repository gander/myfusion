<?php
if (file_exists(INFUSIONS."last_seen_users_panel/locale/".$settings['locale'].".php")) {
	include INFUSIONS."last_seen_users_panel/locale/".$settings['locale'].".php";
} else {
	include INFUSIONS."last_seen_users_panel/locale/English.php";
}

openside($locale['LSUP_000']);
$result = dbquery("SELECT * FROM ".$db_prefix."users ORDER BY user_lastvisit DESC LIMIT 0,10");

echo "<table width='100%' cellpadding='0' cellspacing='0'>\n";
if (dbrows($result) != 0) {
	while ($data = dbarray($result)) {
		// Check if user has ever logged in
		if ($data['user_lastvisit'] != 0) {
			$lastseen = time() - $data['user_lastvisit'];
			$iW=sprintf("%2d",floor($lastseen/604800));
			$iD=sprintf("%2d",floor($lastseen/(60*60*24)));
			$iH=sprintf("%02d",floor((($lastseen%604800)%86400)/3600));
			$iM=sprintf("%02d",floor(((($lastseen%604800)%86400)%3600)/60));
			$iS=sprintf("%02d",floor((((($lastseen%604800)%86400)%3600)%60)));
			if ($lastseen < 60){
				$lastseen="<img src='".INFUSIONS."last_seen_users_panel/images/online.gif' alt='".$locale['LSUP_001']."' title=''>";
			} elseif ($lastseen < 360){
				$lastseen="<img src='".INFUSIONS."last_seen_users_panel/images/offline.gif' alt='".$locale['LSUP_002']."' title=''>";
			} elseif ($iW > 0){
				if ($iW == 1) {
					$Text = $locale['LSUP_003'];
				} else {
					$Text = $locale['LSUP_004'];
				}
				$lastseen = $iW." ".$Text;
			} elseif ($iD > 0){
				if ($iD == 1) {
					$Text = $locale['LSUP_005'];
				} else {
					$Text = $locale['LSUP_006'];
				}
				$lastseen = $iD." ".$Text;
			} else {
				$lastseen = $iH.":".$iM.":".$iS;
			}
		} else {
			$lastseen = $locale['LSUP_007'];
		}
		echo "<tr><td class='side-small' align='left'><a href='".BASEDIR."profile.php?lookup=".$data['user_id']."' class='side'>";
		echo $data['user_name']."</a></td><td class='side-small' align='right' style='white-space:nowrap'>".$lastseen."</td></tr>\n";
	}
}
echo "</table>\n";
closeside();
?>