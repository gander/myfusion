<?php
/*---------------------------------------------------+
| PHP-Fusion 6 Content Management System
+----------------------------------------------------+
| Copyright � 2002 - 2006 Nick Jones
| http://www.php-fusion.co.uk/
+----------------------------------------------------+
| Released under the terms & conditions of v2 of the
| GNU General Public License. For details refer to
| the included gpl.txt file or visit http://gnu.org
+----------------------------------------------------*/
require_once "../maincore.php";
require_once BASEDIR."subheader.php";
require_once ADMIN."navigation.php";
include LOCALE.LOCALESET."admin/settings.php";

if (!checkrights("S6") || !defined("iAUTH") || $aid != iAUTH) fallback("../index.php");

if (isset($_POST['savesettings'])) {
	$maintenance = (isNum($_POST['maintenance']) ? $_POST['maintenance'] : 0);
	if (($userdata['user_id']!=1 && $maintenance==3) || (!iSUPERADMIN && $maintenance==2)) $maintenance = 0;
	
	$result = dbquery("UPDATE ".$db_prefix."settings SET
		tinymce_enabled='".(isNum($_POST['tinymce_enabled']) ? $_POST['tinymce_enabled'] : "0")."',
		smtp_host='".stripinput($_POST['smtp_host'])."',
		smtp_username='".stripinput($_POST['smtp_username'])."',
		smtp_password='".stripinput($_POST['smtp_password'])."',
		bad_words_enabled='".(isNum($_POST['bad_words_enabled']) ? $_POST['bad_words_enabled'] : "0")."',
		bad_words='".addslash($_POST['bad_words'])."',
		bad_word_replace='".stripinput($_POST['bad_word_replace'])."',
		guestposts='".(isNum($_POST['guestposts']) ? $_POST['guestposts'] : "0")."',
		numofshouts='".(isNum($_POST['numofshouts']) ? $_POST['numofshouts'] : "10")."',
		flood_interval='".(isNum($_POST['flood_interval']) ? $_POST['flood_interval'] : "15")."',
		maintenance='".$maintenance."',
		maintenance_message='".addslash(descript($_POST['maintenance_message']))."'
	");
	$result = dbquery("UPDATE ".$db_prefix."settings_extra SET
		allow_username_change='".(isNum($_POST['allow_username_change']) ? $_POST['allow_username_change'] : "1")."',
		avatar_max_width='".(isNum($_POST['avatar_max_width']) ? $_POST['avatar_max_width'] : "100")."',
		avatar_max_height='".(isNum($_POST['avatar_max_height']) ? $_POST['avatar_max_height'] : "100")."',
		avatar_max_kbytes='".(isNum($_POST['avatar_max_kbytes']) ? $_POST['avatar_max_kbytes'] : "30")."',
		download_for_members='".(isNum($_POST['download_for_members']) ? $_POST['download_for_members'] : "1")."',
		news_per_page='".(isNum($_POST['news_per_page']) ? $_POST['news_per_page'] : "11")."',
		sig_maxsize='".(isNum($_POST['sig_maxsize']) ? $_POST['sig_maxsize'] : "500")."',
		header_content='".addslash(trim($_POST['header_content']))."',
		footer_content='".addslash(trim($_POST['footer_content']))."'
	");
	redirect(FUSION_SELF.$aidlink);
}

$settings2 = dbarray(dbquery("SELECT * FROM ".$db_prefix."settings, ".$db_prefix."settings_extra"));

opentable($locale['400']);
require_once ADMIN."settings_links.php";
echo "<form name='settingsform' method='post' action='".FUSION_SELF.$aidlink."'>
<table border='0' align='center' cellpadding='0' cellspacing='0' width='500'>
<tr>
<td width='55%' class='tbl'>".$locale['myf_0020']."</td>
<td width='40%' class='tbl' valign='top' align='left'>
<select name='allow_username_change' class='textbox'>
<option value='1'".($settings2['allow_username_change'] == "1" ? " selected='selected'" : "").">".$locale['508']."</option>
<option value='0'".($settings2['allow_username_change'] == "0" ? " selected='selected'" : "").">".$locale['509']."</option>
</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#allow_username_change' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='55%' class='tbl'>".$locale['myf_0021']."</td>
<td width='40%' class='tbl' valign='top' align='left'>
<select name='avatar_max_width' class='textbox'>\n";
for ($i=50; $i<=125; $i+=5) echo "<option value='".$i."'".($settings2['avatar_max_width'] == $i ? " selected='selected'" : "").">".$i."px</option>\n";
echo "</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#avatar_max_width' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='55%' class='tbl'>".$locale['myf_0022']."</td>
<td width='40%' class='tbl' valign='top' align='left'>
<select name='avatar_max_height' class='textbox'>\n";
for ($i=50; $i<=125; $i+=5) echo "<option value='".$i."'".($settings2['avatar_max_height'] == $i ? " selected='selected'" : "").">".$i."px</option>\n";
echo "</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#avatar_max_height' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='55%' class='tbl'>".$locale['myf_0023']."</td>
<td width='40%' class='tbl' valign='top' align='left'>
<select name='avatar_max_kbytes' class='textbox'>\n";
for ($i=10; $i<=300; $i+=10) echo "<option value='".$i."'".($settings2['avatar_max_kbytes'] == $i ? " selected='selected'" : "").">".$i." kB</option>\n";
echo "</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#avatar_max_kbytes' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='55%' class='tbl'>".$locale['myf_0024']."</td>
<td width='40%' class='tbl' valign='top' align='left'>
<select name='sig_maxsize' class='textbox'>\n";
for ($i=100; $i<=1000; $i+=100) echo "<option value='".$i."'".($settings2['sig_maxsize'] == $i ? " selected='selected'" : "").">".$i."</option>\n";
echo "</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#sig_maxsize' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='55%' class='tbl'>".$locale['myf_0025']."</td>
<td width='40%' class='tbl' valign='top' align='left'>
<select name='download_for_members' class='textbox'>
<option value='0'".($settings2['download_for_members'] == "0" ? " selected='selected'" : "").">".$locale['508']."</option>
<option value='1'".($settings2['download_for_members'] == "1" ? " selected='selected'" : "").">".$locale['509']."</option>
</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#download_for_members' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['662']."<br>
<span class='small2'>".$locale['663']."</span></td>
<td width='50%' class='tbl'><select name='tinymce_enabled' class='textbox'>
<option value='1'".($settings2['tinymce_enabled'] == "1" ? " selected='selected'" : "").">".$locale['508']."</option>
<option value='0'".($settings2['tinymce_enabled'] == "0" ? " selected='selected'" : "").">".$locale['509']."</option>
</select></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#tinymce_enabled' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['664']."<br>
<span class='small2'>".$locale['665']."</span></td>
<td width='50%' class='tbl'><input type='text' name='smtp_host' value='".$settings2['smtp_host']."' maxlength='200' class='textbox' style='width:200px;'></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#smtp_host' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['666']."</td>
<td width='50%' class='tbl'><input type='text' name='smtp_username' value='".$settings2['smtp_username']."' maxlength='100' class='textbox' style='width:200px;'></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#smtp_username' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['667']."</td>
<td width='50%' class='tbl'><input type='password' name='smtp_password' value='".$settings2['smtp_password']."' maxlength='100' class='textbox' style='width:200px;'></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#smtp_password' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['659']."</td>
<td width='50%' class='tbl'><select name='bad_words_enabled' class='textbox'>
<option value='1'".($settings2['bad_words_enabled'] == "1" ? " selected='selected'" : "").">".$locale['508']."</option>
<option value='0'".($settings2['bad_words_enabled'] == "0" ? " selected='selected'" : "").">".$locale['509']."</option>
</select></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#bad_words_enabled' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr><td valign='top' width='50%' class='tbl'>".$locale['651']."<br>
<span class='small2'>".$locale['652']."<br>
".$locale['653']."</span></td>
<td width='50%' class='tbl'><textarea name='bad_words' rows='5' cols='44' class='textbox'>".$settings2['bad_words']."</textarea></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#bad_words' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['654']."</td>
<td width='50%' class='tbl'><input type='text' name='bad_word_replace' value='".$settings2['bad_word_replace']."' maxlength='128' class='textbox' style='width:200px;'></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#bad_word_replace' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['655']."</td>
<td width='50%' class='tbl'><select name='guestposts' class='textbox'>
<option value='1'".($settings2['guestposts'] == "1" ? " selected='selected'" : "").">".$locale['508']."</option>
<option value='0'".($settings2['guestposts'] == "0" ? " selected='selected'" : "").">".$locale['509']."</option>
</select></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#guestposts' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['myf_0026']."</td>
<td width='50%' class='tbl'>
<select name='news_per_page' class='textbox'>\n";
for ($i=1; $i<=21; $i++) echo "<option value='".$i."'".($settings2['news_per_page'] == $i ? " selected='selected'" : "").">".$i."</option>\n";
echo "</select>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#news_per_page' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['656']."</td>
<td width='50%' class='tbl'>
<select name='numofshouts' class='textbox'>\n";
for ($i=5; $i<=50; $i+=5) echo "<option value='".$i."'".($settings2['numofshouts'] == $i ? " selected='selected'" : "").">".$i."</option>\n";
echo "</select>
</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#numofshouts' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['660']."</td>
<td width='50%' class='tbl'><input type='text' name='flood_interval' value='".$settings2['flood_interval']."' maxlength='2' class='textbox' style='width:40px;'> ".$locale['myf_0009']."</td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#flood_interval' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td valign='top' width='100%' colspan='2' class='tbl'>".$locale['myf_0035']."<br><textarea name='header_content' rows='5' cols='90' class='textbox'>".phpentities(stripslashes($settings2['header_content']))."</textarea></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#header_content' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td valign='top' width='100%' colspan='2' class='tbl'>".$locale['myf_0036']."<br><textarea name='footer_content' rows='5' cols='90' class='textbox'>".phpentities(stripslashes($settings2['footer_content']))."</textarea></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#footer_content' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td width='50%' class='tbl'>".$locale['657']."</td>
<td width='50%' class='tbl'><select name='maintenance' class='textbox' style='width:250px;'>
<option value='0'".($settings2['maintenance'] == "0" ? " selected='selected'" : "").">".$locale['503']."</option>
<option value='1'".($settings2['maintenance'] == "1" ? " selected='selected'" : "").">".$locale['502']." - ".$locale['myf_0027']." ".$locale['user2']."</option>
<option value='2'".($settings2['maintenance'] == "2" ? " selected='selected'" : "").">".$locale['502']." - ".$locale['myf_0027']." ".$locale['user3']."</option>
<option value='3'".($settings2['maintenance'] == "3" ? " selected='selected'" : "").">".$locale['502']." - ".$locale['myf_0027']." ".$locale['myf_0028']."</option>
</select></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#maintenance' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td valign='top' width='100%' colspan='2' class='tbl'>".$locale['658']."<br><textarea name='maintenance_message' rows='15' cols='90' class='textbox'>".phpentities(stripslashes($settings2['maintenance_message']))."</textarea></td>
<td width='5%' class='tbl' valign='middle'><a href='".ADMIN."manual.php#maintenance_message' title='".$locale['myf_0010']."' target='_blank'><img src='".IMAGES."info.gif' alt='[i]' border='0'></a></td>
</tr>
<tr>
<td valign='top' width='100%' colspan='2' class='tbl'>
<input type='button' value='b' class='button' style='font-weight:bold;width:25px' onClick=\"addText('maintenance_message', '<b>', '</b>', 'settingsform');\">
<input type='button' value='i' class='button' style='font-style:italic;width:25px' onClick=\"addText('maintenance_message', '<i>', '</i>', 'settingsform');\">
<input type='button' value='u' class='button' style='text-decoration:underline;width:25px' onClick=\"addText('maintenance_message', '<u>', '</u>', 'settingsform');\">
<input type='button' value='link' class='button' style='width:35px' onClick=\"addText('maintenance_message', '<a href=\'', '\' target=\'_blank\'>Link</a>', 'settingsform');\">
<input type='button' value='img' class='button' style='width:35px' onClick=\"addText('maintenance_message', '<img src=\'".str_replace("../","",IMAGES_N)."', '\' style=\'margin:5px\' align=\'left\'>', 'settingsform');\">
<input type='button' value='small' class='button' style='width:40px' onClick=\"addText('maintenance_message', '<span class=\'small\'>', '</span>', 'settingsform');\">
<input type='button' value='small2' class='button' style='width:45px' onClick=\"addText('maintenance_message', '<span class=\'small2\'>', '</span>', 'settingsform');\">
<input type='button' value='alt' class='button' style='width:25px' onClick=\"addText('maintenance_message', '<span class=\'alt\'>', '</span>', 'settingsform');\">
<input type='button' value='".$locale['myf_0006']."' class='button' style='width:100px' onClick=\"maintenance_message_preview()\">
<br>
<select name='setcolor' class='textbox' style='margin-top:5px' onChange=\"addText('maintenance_message', '<span style=\'color:' + this.options[this.selectedIndex].value + '\'>', '</span>', 'settingsform');this.selectedIndex=0;\">
<option value=''>".$locale['myf_0029']."</option>
<option value='maroon' style='color:maroon'>Maroon</option>
<option value='red' style='color:red'>Red</option>
<option value='orange' style='color:orange'>Orange</option>
<option value='brown' style='color:brown'>Brown</option>
<option value='yellow' style='color:yellow'>Yellow</option>
<option value='green' style='color:green'>Green</option>
<option value='lime' style='color:lime'>Lime</option>
<option value='olive' style='color:olive'>Olive</option>
<option value='cyan' style='color:cyan'>Cyan</option>
<option value='blue' style='color:blue'>Blue</option>
<option value='navy' style='color:navy'>Navy Blue</option>
<option value='purple' style='color:purple'>Purple</option>
<option value='violet' style='color:violet'>Violet</option>
<option value='black' style='color:black'>Black</option>
<option value='gray' style='color:gray'>Gray</option>
<option value='silver' style='color:silver'>Silver</option>
<option value='white' style='color:white'>White</option>
</select>
<select name='insertimage' class='textbox' style='margin-top:5px' onChange=\"insertText('maintenance_message', '<img src=\'".$settings['siteurl'].str_replace("../","",IMAGES)."' + this.options[this.selectedIndex].value + '\'>', 'settingsform');this.selectedIndex=0;\">
<option value=''>".$locale['myf_0030']."</option>".makefileopts(makefilelist(IMAGES, ".|..|index.php", true))."</select>
</tr>
<tr>
<td valign='top' width='100%' colspan='2' class='tbl'>
<table class='tbl-border' cellspacing='1' cellpadding='0' width='100%'><tr><td class='tbl1' id='maintenance_message_preview'></td></tr></table>
</td>
<td class='tbl'></td>
</tr>
<tr>
<td align='center' colspan='3' class='tbl'><br>
<input type='submit' name='savesettings' value='".$locale['750']."' class='button'></td>
</tr>
</table>
</form>
<script type='text/javascript' language='JavaScript'>
<!--
function maintenance_message_preview(){
	document.getElementById('maintenance_message_preview').innerHTML = '<center>'+document.forms['settingsform'].elements['maintenance_message'].value+'<\/center>';
}
maintenance_message_preview();
//-->
</script>\n";
closetable();

echo "</td>\n";
require_once BASEDIR."footer.php";
?>