<?php
require_once "../maincore.php";
if (!iADMIN) fallback("../index.php");

if (file_exists(LOCALE.LOCALESET."myf_manual.html")){
	require_once (LOCALE.LOCALESET."myf_manual.html");
} elseif (file_exists(LOCALE."English/myf_manual.html")) {
	require_once(LOCALE."English/myf_manual.html");
} else {
	echo "Manual File not Exists!";
}


?>