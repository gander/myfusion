<?php
/*--------------------------------------------+
| PHP-Fusion v6 - Content Management System   |
|---------------------------------------------|
| author: Nick Jones (Digitanium) � 2002-2005 |
| web: http://www.php-fusion.co.uk            |
| email: nick@php-fusion.co.uk                |
|---------------------------------------------|
| Released under the terms and conditions of  |
| the GNU General Public License (Version 2)  |
+--------------------------------------------*/
/*--------------------------------------------+
|       Fusion3 Theme for PHP-Fusion v6       |
|---------------------------------------------|
| author: PHP-Fusion Themes - Shedrock � 2005 |
| web: http://phpfusion.org                   |
| email: webmaster@phpfusion.org              |
|---------------------------------------------|
| Released under the terms and conditions of  |
| the GNU General Public License (Version 2)  |
+--------------------------------------------*/
if (!defined("IN_FUSION")) { header("Location: ../../index.php"); exit; }
require_once INCLUDES."theme_functions_include.php";

/* Theme Settings */
$body_text = "#000000";
$body_bg = "#8B8B8B";
$theme_width = (isset($settings['theme_width']) && $settings['theme_width']>0 ? ($settings['theme_width']=='100'?'100%':$settings['theme_width']) : 1000);
$theme_width_l = "170";
$theme_width_r = "170";

function render_header($header_content="") {

global $theme_width,$settings;

	echo "<table align='center' class='bodyline' width='$theme_width' cellspacing='0' cellpadding='0' border='0'>";
	echo "<tr><td class='bodyline-top-left' width='30' height='30' nowrap='nowrap'></td><td class='bodyline-top' height='30'></td><td class='bodyline-top-right' width='30' height='30' nowrap='nowrap'></td></tr>\n";
	echo "<tr><td class='bodyline-left' width='30' nowrap='nowrap'></td><td>";

	// Start banner code
	if ($header_content != ""){
		echo "<table width='100%' border='0' cellspacing='0' cellpadding='4'><tr>";
		echo "<td width='60%' height='30'>$header_content</td>";
		echo "</tr></table>";
	}
	// End banner code

	echo "<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr>";
	echo "<td class='sub-header'><table width='100%' border='0' cellpadding='4' cellspacing='0'><tr>";
	echo "<td height='22' class='sub-header'>&nbsp;&nbsp;".showsublinks("<img border='0' src='".THEME."images/divider.gif' alt=''>","white")."</td>";
	echo "<td class='sub-header' width='10%' height='22' style='text-align:right; font-weight:bold;' nowrap='nowrap'>".showsubdate();
	echo "&nbsp;&nbsp;&nbsp;</td></tr></table></td></tr></table>";
	echo "<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>";
	echo "<tr><td></td></tr></table>";
	echo "<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center'>";
	echo "<tr valign='top'>";
	echo "<td valign='middle' align='right'>";
	echo "<table width='100%' cellpadding='4' cellspacing='0' border='0'><tr>";
}

function render_footer($license=false) {
	
global $settings,$locale;

	echo "</tr>\n</table>\n";
	echo "<table cellpadding='2' cellspacing='0' width='100%'><tr>";
	echo "<td>".stripslashes($settings['footer'])."<br>";
	echo "<table cellSpacing='0' cellPadding='2' width='100%' border='0'>";
	echo "<tr><td width='35%' height='22' class='footer' align='left'><div align='left'>";
	if ($license == false) {
		echo "<a href='http://www.myfusion.pl/' target='_blank'>MyFusion</a> - Better <a href='http://www.php-fusion.co.uk/' target='_blank'>PHP-Fusion</a> &copy; 2003-".date("Y");
	}
	echo "</div></td>";
	echo "<td class='footer' width='30%' height='22' align='center'><span style='color:#7C00EC'>Purple</span>Buttons by <a href='http://www.gander.pl/' target='_blank'><b>Gander</b></a></td>";
	echo "<td class='footer' width='35%' height='22' align='right'>";
	echo "<font class='visits'><b>".$settings['counter']." </b></font>".($settings['counter'] == 1 ? $locale['140']."\n" : $locale['141']."\n");
	echo "</td></tr></table></td>";
	echo "</tr></table></td></tr></table>";
	echo "<td class='bodyline-right' width='30' nowrap='nowrap'></td></tr>\n";
	echo "<tr><td class='bodyline-bottom-left' width='30' height='30' nowrap='nowrap'></td><td class='bodyline-bottom' height='30'></td><td class='bodyline-bottom-right' width='30' height='30' nowrap='nowrap'></td></tr>\n";
	echo "</table>";
}

function render_news($subject, $news, $info) {
	echo "<table border='0' cellspacing='0' width='100%' cellpadding='0'>\n";
	echo "<tr>\n";
	echo "<td class='block-top-left' width='14' height='21'></td>\n";
	echo "<td class='block-top' height='21' colspan='2'>".$subject."</td>\n";
	echo "<td class='block-top-right' width='14' height='21'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-left' width='14'></td>\n";
	echo "<td class='side-body' colspan='2'>".$news."</td>\n";
	echo "<td class='block-right' width='14'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-left' width='14'></td>\n";
	echo "<td class='side-body' align='left'>".newsposter($info)."</td>\n";
	echo "<td class='side-body' align='right'>\n";
	echo openform("N",$info['news_id']).newsopts($info,"&middot;").closeform("N",$info['news_id'])."</td>\n";
	echo "<td class='block-right' width='14'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-bottom-left' width='14' height='21'></td>\n";
	echo "<td class='block-bottom' height='21' colspan='2'></td>\n";
	echo "<td class='block-bottom-right' width='14' height='21'></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
}

function render_article($subject, $article, $info) {
	
global $locale;

	echo "<table border='0' cellspacing='0' width='100%' cellpadding='0'><tr>";
	echo "<td class='block-top-left' width='14' height='21'></td>";
	echo "<td class='block-top' height='21'>$subject</td><td class='block-top-right' width='14' height='21'></td>";
	echo "</tr><tr>";
	echo "<td class='block-left' width='14'></td>";
	echo "<td class='side-body'>".($info['article_breaks'] == "y" ? nl2br($article) : $article)."</td><td class='block-right' width='14'></td>";
	echo "</tr><tr><td class='block-left' width='14'></td><td class='side-body'>";
	echo openform("A",$info['article_id']).articleopts($info,"&middot;").closeform("A",$info['article_id']);
	echo "</td><td class='block-right' width='14'></td></tr><tr>";
	echo "<td class='block-bottom-left' width='14' height='21'></td><td class='block-bottom' height='21'></td>";
	echo "<td class='block-bottom-right' width='14' height='21'></td></tr></table>";
}

// Open table begins
function opentable($title) {
	echo "<table border='0' cellspacing='0' width='100%' cellpadding='0'>\n";
	echo "<tr>\n";
	echo "<td class='block-top-left' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "<td class='block-top' height='21'>$title</td>\n";
	echo "<td class='block-top-right' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-left' width='14' nowrap='nowrap'></td>\n";
	echo "<td class='main-body'>\n";
}

// Close table end
function closetable() {
	echo "</td>\n";
	echo "<td class='block-right' width='14' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-bottom-left' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "<td class='block-bottom' height='21'></td>\n";
	echo "<td class='block-bottom-right' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
}

function openside($title) {
	echo "<table border='0' cellspacing='0' width='100%' cellpadding='0'>\n";
	echo "<tr>\n";
	echo "<td class='block-top-left' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "<td class='block-top' height='21'>$title</td>\n";
	echo "<td class='block-top-right' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-left' width='14'></td>\n";
	echo "<td class='side-body'>\n";
}

function closeside() {
	echo "</td>\n";
	echo "<td class='block-right' width='14'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-bottom-left' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "<td class='block-bottom' height='21'></td>\n";
	echo "<td class='block-bottom-right' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	tablebreak();
}

function openside_x($title,$open="on") {
	$boxname = str_replace(" ", "", $title);
	$box_img = $open == "on" ? "off" : "on";
	echo "<table border='0' cellspacing='0' width='100%' cellpadding='0'>\n";
	echo "<tr>\n";
	echo "<td class='block-top-left' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "<td class='block-top' height='21'>$title</td>\n";
	echo "<td class='block-top' align='right' height='21' width='10'><img src='".THEME."images/panel_$box_img.gif' name='b_$boxname' alt='' onclick=\"javascript:flipBox('$boxname')\"></td>\n";
	echo "<td class='block-top-right' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-left' width='14'></td>\n";
	echo "<td class='side-body' colspan='2'><div id='box_$boxname'".($open=="off" ? "style='display:none'" : "").">\n";
}


function opensidex($title,$open="on") {
	static $boxnum = 0;
	$boxname = "opensidex_".$boxnum;
	$box_img = $open == "on" ? "off" : "on";
	echo "<table border='0' cellspacing='0' width='100%' cellpadding='0'>\n";
	echo "<tr>\n";
	echo "<td class='block-top-left' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "<td class='block-top' height='21'>$title</td>\n";
	echo "<td class='block-top' align='right' height='21' width='10'><img src='".THEME."images/panel_$box_img.gif' name='b_$boxname' alt='' onclick=\"javascript:flipBox('$boxname')\"></td>\n";
	echo "<td class='block-top-right' width='14' height='21' nowrap='nowrap'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-left' width='14'></td>\n";
	echo "<td class='side-body' colspan='2'><div id='box_$boxname'".($open=="off" ? "style='display:none'" : "").">\n";
	$boxnum++;
}


function closesidex() {
	echo "</div></td>\n";
	echo "<td class='block-right' width='14'></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class='block-bottom-left' width='14' height='21'></td>\n";
	echo "<td class='block-bottom' height='21' colspan='2'></td>\n";
	echo "<td class='block-bottom-right' width='14' height='21'></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	tablebreak();
}

// Table functions
function tablebreak() {
	echo "<table width='100%' cellspacing='0' cellpadding='0'><tr><td height='8'></td></tr></table>\n";
}
?>